version=$1
#path=../LHE/HH_PWG_FT/
#path=../LHE/HH_MG5aMCatNLO_FTapprox/
#path=../LHE/HH_MG5_LO_forlambda/
#path=../LHE/
path=../hh_mc_gghh_lhe/

# versions for HH_PWG_FT:
#FTapprox-lambda01
# FT-lambda00/  FT-lambda01/  FT-lambda-02.5/  FT-lambda02.5/  FT-lambda-05/  FT-lambda05/  FT-lambda-10/  FT-lambda10/  FT-lambda-15/  FT-lambda15/  FT-lambda-20/  FT-lambda20/

# ../LHE/HH_PWG_FT/
#for ifile in $(ls ${path}/${version}/*.lhe)
# ../LHE/ {others}
for ifile in $(ls ${path}/${version}/*events*)
do
  echo "Converting ..." ${ifile}
  ./ExRootLHEFConverter ${ifile} ${ifile}.root
done

# non-ROOT classes, do NOT hadd, run directly with ana code
#hadd ${path}/${version}.root $(ls ${path}/${version}/*.root)

echo
echo
#echo "Please clean by hand!!!"
ls ${path}/${version}/*.root
#echo "Please clean by hand!!!"
#echo "rm -rf ${path}/${version}/*.root"
