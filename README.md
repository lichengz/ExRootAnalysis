# Introduction

This is an applicaiton of ExRootAnalysis in processing HH LHE files directly.

ExRootAnalysis is a package designed to simplify ROOT tree production and analysis.
It is developped by cp3.
```
https://cp3.irmp.ucl.ac.be/projects/ExRootAnalysis
```

For questions of hh_lhe_root, please contact Xiaohu.Sun@cern.ch


## Setup
Use this setup or higher versions, to be consistent to hh_mc_gghh.
```
lsetup 'lcgenv -p LCG_94 x86_64-slc6-gcc7-opt all'
make
```

## Convert LHE records to trees
The steering file myrun.sh has many examples.
```
. mycvt_lhe_root.sh FT-lambda01
```
In mycvt_lhe_root.sh, the LHE path is setup and the conversion takes place.

## Convert trees to histograms
The steering file myrun.sh has many examples.
```
python my_HH_ana.py FT-lambda01
```
my_HH_ana.py connects the outputs from the last step
and define all histograms to save.

